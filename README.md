# Homotopy Type Theory Exercises

Written solutions to the exercises in the [Homotopy Type Theory book](https://homotopytypetheory.org/book/) and from other resources